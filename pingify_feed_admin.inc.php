<?php

/**
 * @file
 * Cointans functions related to the feed administration.
 */

/**
 * List the feeds in a table, ordered by the title.
 */
function pingify_list_feeds() {
  drupal_set_title('Pingify feeds');
  $result = db_query('SELECT * FROM {pingify_feeds} ORDER BY title');

  // The columns for the table.
  $cols = array(
    t('Feed title'),
    t('URL'),
    t('Operations'),
  );

  // Build the rows for the table.
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $feed_url = url(trim($row->path), array('absolute' => TRUE));
    $rows[] = array(
      check_plain($row->title),
      l($feed_url, $feed_url),
      l(t('edit'), 'admin/settings/pingify/feeds/' . $row->fid . '/edit'),
    );
  }

  // Create the table.
  return theme('table', $cols, $rows);
}

/**
 * Build the form to add or edit a feed.
 *
 * @return
 *   An array that holds the form elements.
 */
function pingify_feed_form(&$form_state, $fid = NULL) {
  // Get the settings.
  $settings = pingify_get_feed_settings($fid);

  // Redirect to the feed list.
  $form['#redirect'] = 'admin/settings/pingify/sites';

  // A hidden field that cointans the fid.
  $form['pingify_fid'] = array(
    '#type' => 'hidden',
    '#value' => $fid,
  );

  // Feed info.
  $form['pingify_feed'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic feed information'),
    '#collapsible' => TRUE,
  );
  $form['pingify_feed']['pingify_feed_title'] = array(
    '#type' => 'textfield',
    '#title' => t('The title of the feed'),
    '#default_value' => $settings['title'],
    '#required' => TRUE,
    '#maxlength' => 255,
  );
  $form['pingify_feed']['pingify_feed_path'] = array(
    '#type' => 'textfield',
    '#title' => t('The path to the feed'),
    '#default_value' => $settings['path'],
    '#description' => t("Enter the Drupal path to the feed, for an example, <em>the-coolest-blog-ever/feed</em>. The path should not begin with a slash."),
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  // Trigger fieldset.
  $form['pingify_trigger'] = array(
    '#type' => 'fieldset',
    '#title' => t('When the feed should be pinged'),
    '#collapsible' => TRUE,
  );

  // The action to trigger the ping.
  $form['pingify_trigger']['pingify_events'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Events that should trigger the ping'),
    '#default_value' => $settings['events'],
    '#options' => array(
      'created' => t('When new content is created'),
      'updated' => t('When existing content is updated'),
      'promoted' => t('When content is promoted to the frontpage'),
      'cron' => t('Everytime cron runs'),
    ),
    '#required' => TRUE,
  );

  // The content type that should trigger the ping.
  $form['pingify_trigger']['pingify_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Only when the content type is'),
    '#default_value' => $settings['content_types'],
    '#options' => pingify_get_content_types(),
    '#required' => TRUE,
  );

  // Buttons.
  $form['pingify_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if (isset($fid)) {
    $form['pingify_delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }

  return $form;
}

/**
 * Validates the feed form.
 *
 * If the feed is being saved, check to see if there is another feed with the same path.
 * If there is, set an error.
 */
function pingify_feed_form_validate($form, &$form_state) {
  // Only validate if the site is being saved.
  if ($form_state['values']['op'] == t('Save')) {
    $fid = $form_state['values']['pingify_fid'];
    $feed_path = $form_state['values']['pingify_feed_path'];
    // Set an error if the feed already exists.
    if ($feed_path != '') {
      if (db_result(db_query("SELECT COUNT(*) FROM {pingify_feeds} WHERE path = '%s' AND fid <> %d", $feed_path, $fid)) > 0) {
        form_set_error('pingify_feed_path', t('The path you have entered already exists in another feed!'));
      }
    }
  }
}

/**
 * Submits the feed form.
 *
 * If the feed is being saved, save the title and path to the database, and save the other
 * settings as variables. If the feed is deleted, delete the database record and related
 * variables.
 */
function pingify_feed_form_submit($form, &$form_state) {
  $fid = $form_state['values']['pingify_fid'];
  // Save the site.
  if ($form_state['values']['op'] == t('Save')) {
    // Create the feed object.
    $feed->fid = $fid;
    $feed->title = $form_state['values']['pingify_feed_title'];
    $feed->path = $form_state['values']['pingify_feed_path'];
    $events = $form_state['values']['pingify_events'];
    $content_types = $form_state['values']['pingify_content_types'];

    if ($feed->fid == '') {
      // Save the feed to the database.
      drupal_write_record('pingify_feeds', $feed);
    }
    else {
      // Update an existing feed.
      drupal_write_record('pingify_feeds', $feed, 'fid');
    }

    // Save the settings as variables.
    variable_set('pingify_events_'. $feed->fid, $events);
    variable_set('pingify_content_types_'. $feed->fid, $content_types);

    drupal_set_message(t('The feed has been saved.'));
  }
  // Delete the site.
  elseif ($form_state['values']['op'] == t('Delete')) {
    db_query("DELETE FROM {pingify_feeds} WHERE fid = %d", $fid);
    variable_del('pingify_events_'. $fid);
    variable_del('pingify_content_types_'. $fid);
    drupal_set_message(t('The feed has been deleted.'));
  }
}