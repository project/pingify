<?php

/**
 * @file
 * Cointans functions related to the site administration.
 */

/**
 * Menu callback, list the sites in a table, ordered by the name.
 */
function pingify_list_sites() {
  $result = db_query('SELECT * FROM {pingify_sites} ORDER BY name');

  // The columns for the table.
  $cols = array(
    t('Site name'),
    t('URL'),
    t('Operations'),
  );

  // Build the rows for the table.
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      check_plain($row->name),
      l($row->url, $row->url),
      l(t('edit'), 'admin/settings/pingify/sites/' . $row->sid . '/edit'),
    );
  }

  // Create the table.
  $content = theme('table', $cols, $rows);
  return $content;
}

/**
 * Build the form to add or edit a site.
 *
 * @return
 *   An array that holds the form elements.
 */
function pingify_site_form(&$form_state, $sid = NULL) {
  // Get the settings.
  $settings = pingify_get_site_settings($sid);

  // A hidden field that cointans the sid.
  $form['pingify_sid'] = array(
    '#type' => 'hidden',
    '#value' => $sid,
  );

  // The site name.
  $form['pingify_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('The name of the site'),
    '#default_value' => $settings['name'],
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  // The site url.
  $form['pingify_site_url'] = array(
    '#type' => 'textfield',
    '#title' => t('The url used to send the ping'),
    '#default_value' => $settings['url'],
    '#description' => t("The url must accept an XML-RPC signal to be able to accept the ping. As an example, the url used to ping Twingly is <em>http://rpc.twingly.com/</em>"),
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  // Buttons.
  $form['pingify_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if (isset($sid)) {
    $form['pingify_delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }

  return $form;
}

/**
 * Validates the site form.
 *
 * If the site is beeing saved, send a ping to the url to see if we'll get
 * a valid response. If we don't, set an error since the url isn't valid.
 */
function pingify_site_form_validate($form, &$form_state) {
  // Only validate if the site is being saved.
  if ($form_state['values']['op'] == t('Save')) {
    $sid = $form_state['values']['pingify_sid'];
    $ping_url = $form_state['values']['pingify_site_url'];
    // Send a ping to see if the url responds correctly.
    if ($ping_url != '') {
      global $base_path;
      $site_url = 'http://'. $_SERVER['HTTP_HOST'] . $base_path;
      $site_name = variable_get('site_name', '');
      if (!xmlrpc($ping_url, 'weblogUpdates.ping', $site_name, $site_url)) {
        form_set_error('pingify_site_url', t("The URL you entered didn't respond correctly! Are you sure it's a valid url?"));
      }
    }
  }
}

/**
 * Submits the site form.
 *
 * If the site is beeing saved, insert/update the database record. If the site
 * is beeing deleted, delete the database record.
 */
function pingify_site_form_submit($form, &$form_state) {
  $sid = $form_state['values']['pingify_sid'];
  // Save the site.
  if ($form_state['values']['op'] == t('Save')) {
    // Create the site object.
    $site->sid = $sid;
    $site->name = $form_state['values']['pingify_site_name']; 
    $$site->url = $form_state['values']['pingify_site_url'];

    if ($site->sid == '') {
      // Save the site to the database.
      drupal_write_record('pingify_sites', $site);
    }
    else {
      // Update an existing site.
      drupal_write_record('pingify_sites', $site, 'sid');
    }
    drupal_set_message(t('The site has been saved.'));
  }
  // Delete the site.
  elseif ($form_state['values']['op'] == t('Delete')) {
    db_query("DELETE FROM {pingify_sites} WHERE sid = %d", $sid);
    drupal_set_message(t('The site has been deleted.'));
  }
  
  $form_state['redirect'] = 'admin/settings/pingify/sites';
}